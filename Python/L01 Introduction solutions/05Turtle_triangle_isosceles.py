from turtle import *


# for an isosceles triangle, the two forward() commands must have the same value
#  put any number you like for the left() command (except left(0))
forward(250)
left(160)
forward(250)
goto(0, 0)

exitonclick()
