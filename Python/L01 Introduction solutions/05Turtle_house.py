# there are lots of ways you could do this!

from turtle import *


fillcolor("red")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
left(120)
end_fill()

fillcolor("yellow")
begin_fill()
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
end_fill()

exitonclick()
