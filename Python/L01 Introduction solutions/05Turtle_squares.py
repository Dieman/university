# draw two squares, side by side, not touching

from turtle import *


forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)

penup()
forward(150)
pendown()

forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)

exitonclick()
