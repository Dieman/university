# draw two squares, side by side, not touching

from turtle import *


def draw_square(length):
    forward(length)
    left(90)
    forward(length)
    left(90)
    forward(length)
    left(90)
    forward(length)
    left(90)


draw_square(100)

penup()
forward(150)
pendown()

draw_square(75)

penup()
forward(125)
pendown()

draw_square(50)

penup()
forward(100)
pendown()

draw_square(25)

exitonclick()

print()
input("Press return to continue ...")
